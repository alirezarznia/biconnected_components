//#include <GOD>
#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>
#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP 			make_pair
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	piii 		pair<ll,pii>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62
#define     S           second
#define     F           first
typedef long long ll;
using namespace std;
vector<ll>vec[101];
ll disc[101];
ll low[101];
ll tim=0;
bool ans;
bool DFS(ll x , ll p){
    disc[x]=low[x]=++tim;
    ll ch =0;
    for(auto i : vec[x]){
        if(disc[i]== -1){
            ch++;
            DFS(i , x);
            low[x]=min(low[i] , low[x]);
            if(p==-1 && ch>1) {ans =true;return false;}
            if(p!=-1 && low[i]>=disc[x]) {ans =true; return false;}
        }
        else if(i != p) low[x]=min(low[x] , disc[i]);
    }
    return true;
}
int main(){
//    Test;
    ll t; cin >>t;
    while(t--){
        ll n , m ;cin>>n>>m;
        Rep(i ,n) vec[i].clear();
        Rep(i , m){
            ll x, y ; cin >> x>>y; vec[x].push_back(y); vec[y].push_back(x);
        }
        ans= false;
        Set(disc ,-1);
           DFS(0 ,-1);
           if(!ans){
        Rep(i ,n){
            if(disc[i]== -1){
                ans=true; break;
            }
        }
        }
        if(!ans) cout<<1<<endl;
        else cout<<0<<endl;
    }
}
